#include "Player.hpp"
#include <iostream>

Player::Player() : Player("undefined", '.', 0){};

Player::Player(std::string name, char color, int id) : name(name), color(color), id(id), canPlay(true), score(0), placementsNumber(0), pieces(62, Piece(color)){
    initPlayerPieces();
};

char Player::getColor(){
    return color;
};

int Player::getId(){
    return id;
};

Piece Player::getPiece(){
    /** retourne un pion de joueur avec suppression. */
    if(!pieces.empty()){
        Piece tmpPiece = pieces.back();
        pieces.pop_back();
        return tmpPiece;
    }
    else{ /** Si le joueur ne possède plus de pièce, il ne peut pas jouer. */
        setCanPlay(false);
        return Piece('.');
    }
};

int Player::getRemainingPieces(){
    return pieces.size(); /** Retourne la taille du vecteur. */
};

void Player::setCanPlay(bool _canPlay){
    canPlay = _canPlay;
};
bool Player::getCanPlay(){
    return canPlay;
};

void Player::setScore(int _score){
    score = _score;
};

int Player::getScore(){
    return score;
};

std::string Player::getName(){
    return name;
}

void Player::setPlacementsNumber(int _placementsNumber){
    placementsNumber = _placementsNumber;
};

int Player::getPlacementsNumber(){
    return placementsNumber;
};

void Player::incrementPlacementsNumber(){
    placementsNumber++;
};

void Player::initPlayer(){
    /** Initialisation des attributs d'un joueur. */
    setCanPlay(true);
    setScore(0);
    setPlacementsNumber(0);
    initPlayerPieces();
};

void Player::initPlayerPieces(){
    pieces = std::vector<Piece>(62,Piece(getColor()));
};