#include "Player.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupPlayer) { };

/** 
 * @brief Vérification du constructeur par défaut d'un joueur.
 * 
 * Un joueur est crée, on vérifie sa "couleur" à l'initialisation par défaut.
 * On vérifie son ID.
 * On vérifie également qu'il possède toujours 64 jetons.
 */
TEST(GroupPlayer, TestPlayer_1)
{
    Player playerEmpty;
    CHECK_EQUAL(playerEmpty.getColor(), '.');
    CHECK_EQUAL(playerEmpty.getId(), 0);
    CHECK_EQUAL(playerEmpty.getRemainingPieces(), 62);
};


/** 
 * @brief Vérification de l'état du joueur
 */
TEST(GroupPlayer, TestPlayer_2)
{
    Player player("test", 'B', 1);
    CHECK_EQUAL(player.getCanPlay(), true);
    player.setCanPlay(false);
    CHECK_EQUAL(player.getCanPlay(), false);
};

/** 
 * @brief Vérification du score du joueur
 */
TEST(GroupPlayer, TestPlayer_3)
{
    Player player("test", 'B', 1);
    CHECK_EQUAL(player.getScore(), 0);
    player.setScore(10);
    CHECK_EQUAL(player.getScore(), 10);
};