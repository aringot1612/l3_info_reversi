#pragma once
#include <string>
#include <vector>
#include "../piece/Piece.hpp"

class Player{
    
    private:

        /** @brief String permettant de donner un nom à notre joueur */
        std::string name;

        /** @brief Caractère représentant la couleur du joueur, il s'agit également de la couleur utilisé pour ses pions. */
        char color;

        /** @brief Entier renseignant l'identifiant du joueur. */
        int id;

        /** @brief Booléen renseignant l'état du joueur (true -> il peut jouer | false -> Il passe son tour). */
        bool canPlay;

        /** @brief Score du joueur : nombre de pions de sa couleur présents sur la grille de jeu. */
        int score;

        /** @brief Nombre de pions que le joueur pourra placer à l'instant présent, est régulièrement mis à 0. */
        int placementsNumber;

        /** @brief Pions possédés par le joueur. */
        std::vector<Piece> pieces;

        /** @brief Permet de remplir le vector pieces rapidement. */
        void initPlayerPieces();
        
    public:

        /** @brief Constructeur par défaut, crée un joueur dont le pion sera de couleur ".". */
        Player();

        /** @brief Constructeur avec paramètre.
         * 
         * @param name : le nom du joueur
         * @param color : la couleur de pion pour ce joueur.
         * @param id : l'id du joueur 
         **/
        Player(std::string name, char color, int id);

        /** 
         * @brief Permet de récupérer un pion du joueur pour le poser sur la grille de jeu.
         * Cette méthode se charge de supprimer le pion de la liste des pions du joueur.
         * 
         * @return un pion du joueur
         **/
        Piece getPiece();

        /**
         * @brief Permet de récupèrer le nombre de pions restants pour le joueur.
         * 
         * @return le nombre de pions restants pour ce joueur.
         */
        int getRemainingPieces();

        /** 
         * @brief Permet de récupérer la couleur de ce joueur.
         * 
         * @return le caractère représentant la couleur du joueur.
         **/
        char getColor();
        
        /** 
         * @brief Permet de récupérer l'ID du joueur.
         * 
         * @return l'id du joueur.
         */
        int getId();

        /** 
         * @brief Setter permettant de renseigner l'état du joueur.
         * 
         * @param _canPlay : la valeur booléenne à modifier.
         **/
        void setCanPlay(bool _canPlay);

        /** 
         * @brief Getter permettant de récupèrer l'état du joueur : peut-il jouer ce tour ?
         * 
         * @return : la valeur du booléen canPlay. 
         **/
        bool getCanPlay();

        /**
         * @brief Setter permetttant de renseigner le score du joueur.
         * 
         * @param : le nouveau score du joueur.
        */
        void setScore(int _score);

        /**
         * @brief Getter permettant de récupèrer le score du joueur.
         * 
         * @return : le score du joueur.
         */
        int getScore();

        /**
         * @brief Getter permettant de récupèrer le nom du joueur.
         * 
         * @return : le nom du joueur.
         **/ 
        std::string getName();
        /**
         * @brief Setter permetttant de renseigner le nombre de placements de pions que le joueur peut effectuer.
         * 
         * @param : le nombre de placements de pions du joueur.
        */
        void setPlacementsNumber(int _placementsNumber);

        /**
         * @brief Getter permettant de récupèrer le nombre de placements de pions que le joueur peut effectuer.
         * 
         * @return : le nombre de placements de pions.
         **/ 
        int getPlacementsNumber();

        /**
         * @brief Permet d'incrémenter directement le nombres de coups possible.
         * Est utilisé par board.checkCorrectPlacements() directement.
         **/
        void incrementPlacementsNumber();

        /** @brief Méthode permettant d'initialiser les propriétés du joueur. */
        void initPlayer();
};