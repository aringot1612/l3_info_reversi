#pragma once
#include <iostream>
#include <array>
#include "../piece/Piece.hpp"
#include "../player/Player.hpp"

class Board{
    
    private:
        /** @brief Array - Deux dimensions, remplis de pions */
        std::array<std::array<Piece, 8>, 8> gameBoard;

        /**
         * @brief Méthode privée permettant de vérifier qu'une coordonnée spécifique ne sorte pas d'un tableau.
         * Utilisée uniquement dans checkCorrectPlacements.
         * Cette méthode permet simplement de rendre la méthode checkCorrectPlacements plus lisible.
         * 
         * @param row : la ligne correspondante.
         * @param column : la colonne correspondante.
         * @param array : le tableau à vérifier.
         * 
         * @return true si les coordonnées sont correctes, false sinon.
         **/ 
        bool checkTabLimits(unsigned long int row, unsigned long int column, std::array<std::array<Piece, 8>, 8> array);

    public:
        /** @brief Constructeur */
        Board();

        /** @brief Destructeur */
        ~Board();

        /**
         * @brief Permet de renvoyer notre grille de jeu sous la forme d'une chaine de caractère.
         * Cette opérateur n'est utile que pour l'affichage du jeu en ligne de commande.
         * 
         * @param os Ref ostream, valeur qui sera retournée par la même occasion.
         * @param board Référence vers notre grille de jeu.
         * 
         * @return La chaine std::ostream, permettant d'afficher le tableau.
         */
        friend std::ostream & operator<<(std::ostream & os, Board const& board);

        /** @brief Permet de placer quatres jetons dans notre grille de jeu en respectant les règles du Reversi. */
        void initBoard();

        /**
         * @brief Getter permettant de récupérer la couleur d'un pion dans notre grille, en précisant ses coordonnées 2D.
         * 
         * @param row Numéro de ligne (première ligne de la grille -> 0).
         * @param column Numéro de colonne (première colonne de la grille -> 0).
         * 
         * @return la couleur du pion sélectionné.
         **/
        char getBoardCell(unsigned long int row, unsigned long int column);

        /**
         * @brief Permet de placer un pion sur la grille de jeu.
         * 
         * @param piece Pion à placer.
         * @param row numéro de ligne pour notre pion.
         * @param column numéro de colonne pour notre pion.
         * 
         * @return un booléen spécifiant si l'opération s'est bien passée ou non.
         **/
        bool placePiece(Piece piece, unsigned long int row, unsigned long int column);

        /**
         * @brief Permet de vérifier les coups qu'un joueur est capable de porter sur la grille.
         * 
         * @param player : Le joueur courant, cela nous permet d'ajuster l'algo selon la couleur du pion à placer.
         * 
         * @return un tableau à deux dimensions, avec une taille égale à notre grille de jeu. ce tableau contient des 1 ou des 0 (1 -> coup possible | 0 -> coup impossible).
         **/
        std::array<std::array<int, 8>, 8> checkCorrectPlacements(Player* player);

        /**
         * @brief Permet d'analyser les pions proches d'un pion aux coordonnées fournies dans la grille de jeu.
         * Cette méthode ressemble à checkCorrectPlacements().
         * 
         * Lorsqu'elle est appelée, on sait très bien qu'au moins un pion doit changer de couleur (sinon le joueur n'aurait pas pu porter le coup en question).
         * 
         * @param row : la ligne correspondante à la coordonnée.
         * @param column : la colonne correspondante à la coordonnée.
         **/
        void analyzeSurroundingPieces(unsigned long int row, unsigned long int column);

        /**
         * @brief Permet de récupèrer le nombre de pions actuellement présents sur la grille de jeu selon sa couleur.
         * 
         * @param color : la couleur utilisée pour vérifier le nombre de pions présents.
         * 
         * @return Le nombre de pions présents correspondant à la couleur.
         * 
         **/
        int getPiecesNumber(char color);
};