#include "Board.hpp"
#include <iostream>
#include <sstream>

Board::Board(){
   initBoard();
};

Board::~Board(){}

std::ostream& operator<<(std::ostream & os, Board const& board){
    /**
     * Cet opérateur permet d'afficher la grille de jeu dans la console rapidement.
     * Pour cela, nous parcourons le tableau.
     * A chaque ligne, on place les valeurs dans la chaine de sortie.
     * Avant chaque nouvelle ligne, on place un caractère de retour chariot.
     * 
     * Cette méthode est utilisée uniquement pour l'affichage console.
     * Elle est bien entendu inutile pour l'affichage graphique.
     */
    for(auto& rows: board.gameBoard)
    {
        for(auto cell: rows)
            os << cell.getColor() << ' ';
        os << std::endl;
    }
   return os;
};

char Board::getBoardCell(unsigned long int row, unsigned long int column){
    /** Pour récupèrer la couleur d'un pion, 
     * Nous avons juste besoin de connaitre ses coordonnées dans le tableau.
     * On fait appel à la méthode getCouleur pour récupèrer la couleur du pion choisi.
     * 
     * Si on dépassse du tableau, on renvoi une couleur ".*, un pion vide.... */
    if(row < gameBoard.size())
        if(column < gameBoard[row].size())
            return gameBoard[row][column].getColor();
    return '.';
};

bool Board::placePiece(Piece piece, unsigned long int row, unsigned long int column){
    /** Pour placer un pion, nous n'avons qu'à fournir le pion en paramètre, 
     * et le placer aux coordonnées choisis. */
    if(row < gameBoard.size())
        if(column < gameBoard[row].size()){
            gameBoard[row][column] = piece;
            return true;
        }
    return false;
};

 void Board::initBoard(){
     /** Ici, on initialise le tableau avec des pions "vide".
      * Et on place les pions pour le début de jeu.
      */
     for(auto& row: gameBoard)
        for(auto& cell: row)
            cell = Piece('.');

     placePiece(Piece('W'), 3, 3);
     placePiece(Piece('B'), 3, 4);
     placePiece(Piece('B'), 4, 3);
     placePiece(Piece('W'), 4, 4);
 };

 std::array<std::array<int, 8>, 8> Board::checkCorrectPlacements(Player* player){
     /** On crée temporairement un pion selon le joueur courant. */
     Piece piece(player->getColor());

     /** On crée un pion "vide" */
     Piece emptyPiece;

     /** Création d'un pion représentant le joueur adverse. */
     Piece oppositePiece;

     /** Création de deux pions génériques */
     Piece blackPiece('B');
     Piece whitePiece('W');

     unsigned long int index = 1;
     if(piece.getColor() =='W'){
         oppositePiece = blackPiece;
     }else{
         oppositePiece = whitePiece;
     }

     /** Table temporaire pour le mapping des coups possibles */
     std::array<std::array<int, 8>, 8> array;

     /** Le joueur courant a 0 possibilité de placement. */
     player->setPlacementsNumber(0);

    /** initialisation du tableau de mapping. */
    for(auto& row: array)
        for(auto& cell: row)
            cell = 0;

    for(unsigned long int row = 0 ; row < gameBoard.size() ; row ++){
        for(unsigned long int column = 0 ; column < gameBoard[row].size() ; column ++){ /** Double boucle : pour chaque case de la grille de jeu.... */
            index  = 1;
            if(gameBoard[row][column].getColor() == emptyPiece.getColor() && array[row][column] == 0){ /** Si cette case est vide.... */

                /** Vérification des cases situés sous la case concerné. */
                index = 1 ;
                while (checkTabLimits(row + index, column, gameBoard) && gameBoard[row + index][column].getColor() == oppositePiece.getColor()) /**tant que la case située en dessous est d'une couleur opposée à la couleur du joueur.... */
                {
                    index++;
                    if(checkTabLimits(row + index, column, gameBoard)){ /** On s'assure de ne pas dépasser du tableau. */
                        if(gameBoard[row + index][column].getColor() == piece.getColor()){ /** Si on tombe sur une case de notre couleur... */
                            array[row][column] = 1; /** on considére que la case ciblée peut être jouée */
                            player->incrementPlacementsNumber(); /** Le joueur est capable de jouer au moins un coup. */
                        }
                    }
                }

                /** Vérification des cases sur la droite de la case concernée. */
                index = 1 ;
                while (checkTabLimits(row, column + index, gameBoard) && gameBoard[row][column + index].getColor() == oppositePiece.getColor())
                {
                    index++;
                    if(checkTabLimits(row, column + index, gameBoard)){
                        if(gameBoard[row][column + index].getColor() == piece.getColor()){
                            array[row][column] = 1;
                            player->incrementPlacementsNumber();
                        }
                    }
                }

                /** Vérification des cases en diagonale Inférieur Droite de la case concernée. */
                index = 1 ;
                while (checkTabLimits(row + index, column + index, gameBoard) && gameBoard[row + index][column + index].getColor() == oppositePiece.getColor())
                {
                    index++;
                    if(checkTabLimits(row + index, column + index, gameBoard)){
                        if(gameBoard[row + index][column + index].getColor() == piece.getColor()){
                            array[row][column] = 1;
                            player->incrementPlacementsNumber();
                        }
                    }
                }

                /** Vérification des cases au dessus de la case concernée. */
                index = 1 ;
                while (checkTabLimits(row - index, column, gameBoard) && gameBoard[row - index][column].getColor() == oppositePiece.getColor())
                {
                    index++;
                    if(checkTabLimits(row - index, column, gameBoard)){
                        if(gameBoard[row - index][column].getColor() == piece.getColor()){
                            array[row][column] = 1;
                            player->incrementPlacementsNumber();
                        }
                    }
                }

                /** Vérification des cases à gauche de la case concernée. */
                index = 1 ;
                while (checkTabLimits(row, column - index, gameBoard) && gameBoard[row][column - index].getColor() == oppositePiece.getColor())
                {
                    index++;
                    if(checkTabLimits(row, column - index, gameBoard)){
                        if(gameBoard[row][column - index].getColor() == piece.getColor()){
                            array[row][column] = 1;
                            player->incrementPlacementsNumber();
                        }
                    }
                }

                /** Vérification des cases en diagonale Supérieur Gauche de la case concernée. */
                index = 1 ;
                while (checkTabLimits(row - index, column - index, gameBoard) && gameBoard[row - index][column - index].getColor() == oppositePiece.getColor())
                {
                    index++;
                    if(checkTabLimits(row - index, column - index, gameBoard)){
                        if(gameBoard[row - index][column - index].getColor() == piece.getColor()){
                            array[row][column] = 1;
                            player->incrementPlacementsNumber();
                        }
                    }
                }   

                /** Vérification des cases en diagonale Supérieur Droite de la case concernée. */
                index = 1 ;
                while (checkTabLimits(row - index, column + index, gameBoard) && gameBoard[row - index][column + index].getColor() == oppositePiece.getColor())
                {
                    index++;
                    if(checkTabLimits(row - index, column + index, gameBoard)){
                        if(gameBoard[row - index][column + index].getColor() == piece.getColor()){
                            array[row][column] = 1;
                            player->incrementPlacementsNumber();
                        }
                    }
                }

                /** Vérification des cases en diagonale Inférieur Gauche de la case concernée. */
                index = 1 ;
                while (checkTabLimits(row + index, column - index, gameBoard) && gameBoard[row + index][column - index].getColor() == oppositePiece.getColor())
                {
                    index++;
                    if(checkTabLimits(row + index, column - index, gameBoard)){
                        if(gameBoard[row + index][column - index].getColor() == piece.getColor()){
                            array[row][column] = 1;
                            player->incrementPlacementsNumber();
                        }
                    }
                }
            }
        }
    } /** Si le joueur est incapable de jouer un pion... */
    if(player->getPlacementsNumber() < 1)
        player->setCanPlay(false);

    /** On retourne le mapping de placements de pion. */
    return array;
 };



 void Board::analyzeSurroundingPieces(unsigned long int row, unsigned long int column){
    /** Analyse des pions dans les huits directions pour les retourner. */

    /** Création d'un tableau temporaire par copie. */
    std::array<std::array<Piece, 8>, 8> tmpBoard = gameBoard;

    Piece currentPiece;
    Piece oppositePiece;
    Piece blackPiece('B');
    Piece whitePiece('W');

    unsigned long int index = 1;

    if(gameBoard[row][column].getColor() =='W'){ /** Mise en place des pions */
        currentPiece = whitePiece;
        oppositePiece = blackPiece;
    }else{
        currentPiece= blackPiece;
        oppositePiece = whitePiece;
    }

    index = 1 ;
    tmpBoard = gameBoard; /** initialisation du tableau temporaire. */

    /** Vérification des pions situés sous le pion cible */
    while (checkTabLimits(row + index, column, tmpBoard) && tmpBoard[row + index][column].getColor() == oppositePiece.getColor()) /** Si le pion est d'une couleur opposée au pion cible */
    {
        tmpBoard[row + index][column].changeColor(); /** On le retourne (opération réalisée sur le tableau temporaire). */
        index++;
        if(checkTabLimits(row + index, column, tmpBoard)){ /** Vérification des bornes de la grille de jeu */
            if(gameBoard[row + index][column].getColor() == currentPiece.getColor()){ /** Si on retrouve le pion cible en parcourant dans la même direction... */
                gameBoard = tmpBoard; /** On applique le retournement des pions dans la véritable grille de jeu. */
            }
        }
    }

    /** Vérification des pions situés à la droite du pion cible. */
    index = 1 ;
    tmpBoard = gameBoard;
    while (checkTabLimits(row, column + index, tmpBoard) && tmpBoard[row][column + index].getColor() == oppositePiece.getColor())
    {
        tmpBoard[row][column + index].changeColor();
        index++;
        if(checkTabLimits(row, column + index, tmpBoard)){
            if(gameBoard[row][column + index].getColor() == currentPiece.getColor()){
                gameBoard = tmpBoard;
            }
        }
    }

    /** Vérification des pions situés en diagonale Inférieur Droite du pion cible. */
    index = 1 ;
    tmpBoard = gameBoard;
    while (checkTabLimits(row + index, column + index, tmpBoard) && tmpBoard[row + index][column + index].getColor() == oppositePiece.getColor())
    {
        tmpBoard[row + index][column + index].changeColor();
        index++;
        if(checkTabLimits(row + index, column + index, tmpBoard)){
            if(gameBoard[row + index][column + index].getColor() == currentPiece.getColor()){
                gameBoard = tmpBoard;
            }
        }
    }

    /** Vérification des pions situés au dessus du pion cible. */
    index = 1 ;
    tmpBoard = gameBoard;
    while (checkTabLimits(row - index, column, tmpBoard) && tmpBoard[row - index][column].getColor() == oppositePiece.getColor())
    {
        tmpBoard[row - index][column].changeColor();
        index++;
        if(checkTabLimits(row - index, column, tmpBoard)){
            if(gameBoard[row - index][column].getColor() == currentPiece.getColor()){
                gameBoard = tmpBoard;
            }
        }
    }

    /** Vérification des pions situés à gauche du pion cible. */
    index = 1 ;
    tmpBoard = gameBoard;;
    while (checkTabLimits(row, column - index, tmpBoard) && tmpBoard[row][column - index].getColor() == oppositePiece.getColor())
    {
        tmpBoard[row][column - index].changeColor();
        index++;
        if(checkTabLimits(row, column - index, tmpBoard)){
            if(gameBoard[row][column - index].getColor() == currentPiece.getColor()){
                gameBoard = tmpBoard;
            }
        }
    }

    /** Vérification des pions situés en diagonale Supérieur Gauche du pion cible. */
    index = 1 ;
    tmpBoard = gameBoard;
    while (checkTabLimits(row - index, column - index, tmpBoard) && tmpBoard[row - index][column - index].getColor() == oppositePiece.getColor())
    {
        tmpBoard[row - index][column - index].changeColor();
        index++;
        if(checkTabLimits(row - index, column - index, tmpBoard)){
            if(gameBoard[row - index][column - index].getColor() == currentPiece.getColor()){
                gameBoard = tmpBoard;
            }
        }
    }

    /** Vérification des pions situés en diagonale Supérieur Droite du pion cible. */
    index = 1 ;
    tmpBoard = gameBoard;
    while (checkTabLimits(row - index, column + index, tmpBoard) && tmpBoard[row - index][column + index].getColor() == oppositePiece.getColor())
    {
        tmpBoard[row - index][column + index].changeColor();
        index++;
        if(checkTabLimits(row - index, column + index, tmpBoard)){
            if(gameBoard[row - index][column + index].getColor() == currentPiece.getColor()){
                gameBoard = tmpBoard;
            }
        }
    }

    /** Vérification des pions situés en diagonale Inférieur Gauche du pion cible. */
    index = 1 ;
    tmpBoard = gameBoard;
    while (checkTabLimits(row + index, column - index, tmpBoard) && tmpBoard[row + index][column - index].getColor() == oppositePiece.getColor())
    {
        tmpBoard[row + index][column - index].changeColor();
        index++;
        if(checkTabLimits(row + index, column - index, tmpBoard)){
            if(gameBoard[row + index][column - index].getColor() == currentPiece.getColor()){
                gameBoard = tmpBoard;
            }
        }
    }
 }

  bool Board::checkTabLimits(unsigned long int row, unsigned long int column, std::array<std::array<Piece, 8>, 8> array){
    /** On s'assure ici que les coordonnées passées en paramètre soient bien incluses dans le tableau. */
    if(row < array.size()){
        if(column < array[row].size()){
            return true;
        }
    }    
    return false;
 };

 int Board::getPiecesNumber(char color){
     /** On parcourt tout le tableau afin de récupèrer le nombre de pions d'une couleur spécifique.  */
     int counter = 0;
     for(auto& row: gameBoard){
        for(auto& cell: row){
            if(cell.getColor() == color){
                counter++;
            }
        }
     }
     return counter;
 }