#include <iostream>
#include <sstream>
#include "Board.hpp"
#include "../piece/Piece.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupBoard) { };

/** 
 * @brief Vérification de la grille après initialisation du jeu.
 */
TEST(GroupBoard, TestBoard_5)
{
    Board board;
    board.initBoard();
    for(int row = 0 ; row < 8 ; row++)
    {
        for(int column = 0 ; column  < 8 ; column ++)
        {
            if((row == 3 && column == 3) || (row == 4 && column == 4)){
                CHECK_EQUAL(board.getBoardCell(row, column), 'W');
            }
            else{
                if((row == 3 && column == 4) || (row == 4 && column == 3)){
                    CHECK_EQUAL(board.getBoardCell(row, column), 'B');
                }
                else{
                    CHECK_EQUAL(board.getBoardCell(row, column), '.');
                } 
            }
        }
    }
};

/** 
 * @brief Vérification de l'opérator <<
 * 
 * Comparaison de la chaine de caractère renvoyée par '<< board' avec la chaine de caractère normalement obtenue.
 */
TEST(GroupBoard, TestBoard_2)
{
    Board board;
    std::stringstream out;
    out << board;
    STRCMP_EQUAL((out.str()).c_str(), ". . . . . . . . \n. . . . . . . . \n. . . . . . . . \n. . . W B . . . \n. . . B W . . . \n. . . . . . . . \n. . . . . . . . \n. . . . . . . . \n");
};

/** 
 * @brief Vérification de la grille après l'ajout d'un pion.
 * On place un pion prédéfini avec comme Couleur "B", aux coordonnées (0;0).
 * 
 * On vérifie ensuite le contenu de cette case pour s'assurer que le pion a été modifié.
 */
TEST(GroupBoard, TestBoard_3)
{
    Board board;
    Piece piece('B');
    board.placePiece(piece, 0, 0);
    CHECK_EQUAL(board.getBoardCell(0, 0), 'B');
};

/** 
 * @brief Vérification de la grille après l'ajout d'un pion à des coordonnées incorrectes.
 * On place un pion prédéfini avec comme Couleur "W", aux coordonnées (8;8) -> cellule inexistante
 * 
 * Le test vérifie que la méthode retourne bien false.
 */
TEST(GroupBoard, TestBoard_4)
{
    Board board;
    Piece piece('W');
    CHECK_FALSE(board.placePiece(piece, 8, 8));
};



/** 
 * @brief Vérification de la grille des coups possibles.
 * 
 * On initialise une grille de jeu,
 * On crée un pion à poser rapidement.
 * 
 * On fait appel à checkCorrectPlacements pour récupèrer tous les coups possible.
 * 
 * On s'assure finalement que les coups possibles soient correctes.
 */
TEST(GroupBoard, TestBoard_6)
{
    Board board;
    Player playerB("Joueur 1", 'B', 1);
    std::array<std::array<int, 8>, 8> correctPlacements;
    board.initBoard();
    correctPlacements = board.checkCorrectPlacements(&playerB);

    for(int row = 0 ; row < 8 ; row++)
    {
        for(int column = 0 ; column  < 8 ; column ++)
        {
            if((row == 2 && column == 3) || (row == 3 && column == 2) || (row == 4 && column == 5) || (row == 5 && column == 4)){
                CHECK_EQUAL(correctPlacements[row][column], 1);
            }
            else{
                CHECK_EQUAL(correctPlacements[row][column], 0);
            }
        }
    }
};

/** 
 * @brief Vérification du changement de couleur d'un jeton.
 * 
 * On initialise une grille de jeu,
 * On crée un pion à poser rapidement.
 * 
 * On s'assure que la couleur destinéee à être modifié est bien initialisé.
 * On pose le pion à un endroit spécifique du tableau, cet endroit fait parti des coups possible en conditions réelles.
 * 
 * On fait ensuite appel à analyzeSurroundingPieces, qui va analyser les pions alentours.
 * Cette méthode va trouver un pion devant changer de couleur : le pion de coordonnnées (3;3).
 * 
 * On s'assure finalement que la couleur du pion en question a bien été modifiée.
 */
TEST(GroupBoard, TestBoard_7)
{
    Board board;
    Piece pieceB('B');

    board.initBoard();
    CHECK_EQUAL(board.getBoardCell(3, 3), 'W');

    board.placePiece(pieceB, 3, 2);
    board.analyzeSurroundingPieces(3, 2);

    CHECK_EQUAL(board.getBoardCell(3, 3), 'B');
};

/** 
 * @brief On s'assure ici que analyzeSurroundingPieces est capable de modifier la couleur de plusieurs pions.
 * 
 * Le test ressemble donc au précédent.
 * Nous faisons juste en sorte de créer la situation de jeu : le joueur vient de capturer deux pions.
 */
TEST(GroupBoard, TestBoard_8)
{
    Board board;

    Piece pieceB('B');
    Piece pieceW('W');

    board.initBoard();
    board.placePiece(pieceW, 3, 2);

    CHECK_EQUAL(board.getBoardCell(3, 2), 'W');
    CHECK_EQUAL(board.getBoardCell(3, 3), 'W');

    board.placePiece(pieceB, 3, 1);
    CHECK_EQUAL(board.getBoardCell(3, 1), 'B');

    board.analyzeSurroundingPieces(3, 1);

    CHECK_EQUAL(board.getBoardCell(3, 2), 'B');
    CHECK_EQUAL(board.getBoardCell(3, 3), 'B');
};

/** 
 * @brief Vérification du compteur de pions.
 */
TEST(GroupBoard, TestBoard_9)
{
    Board board;
 
    Piece pieceB('B');
    Piece pieceW('W');

    board.initBoard();

    board.placePiece(pieceB, 0, 0);
    board.placePiece(pieceB, 0, 1);
    board.placePiece(pieceW, 1, 0);
    board.placePiece(pieceW, 1, 1);
    board.placePiece(pieceW, 1, 2);
    board.placePiece(pieceW, 1, 3);

    CHECK_EQUAL(board.getPiecesNumber(pieceB.getColor()), 4);
    CHECK_EQUAL(board.getPiecesNumber(pieceW.getColor()), 6);
};