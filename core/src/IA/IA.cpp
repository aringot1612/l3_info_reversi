#include <cstdlib>
#include <iostream>
#include <ctime>
#include "IA.hpp"

#define MONTECARLO_DEFAULT 32

IA::IA(){
    /** Initialisation du jeu de comparaison d'IA. */
    bot1 = Player("Bot 1", 'B', 1); /** Création des joueurs par défaut. */
    bot2 = Player("Bot 2", 'W', 2);
    currentPlayer = &bot1; /** Le joueur 1 joue en premier. */
    gameIsOver = false;
};
IA::~IA(){};

void IA::startGame(int argc, char *argv[]){
    /** Permet de gérer l'aléatoire. */
    srand((unsigned) time(0));

    /** Si le nombre de paramètres passés est inférieur à 5, inutile de continuer. */
    if(argc < 5)
        return;

    /** Index de parcourt de choix. */
    unsigned long int index = 0;

    /** Nombre de game IA vs IA à lancer. */
    int occurencesNumber = atoi(argv[1]);

    /** Type d'IA du joueur 1. */
    char * player1Type = argv[2];

    /** Paramètre de l'IA 1, utile uniquement si l'IA est de type mc : Monte Carlo. */
    int player1Parameter = atoi(argv[3]);

    /** Type d'IA du joueur 2. */
    char * player2Type = argv[4];

    /** Paramètre de l'IA 2, utile uniquement si l'IA est de type mc : Monte Carlo. */
    int player2Parameter = atoi(argv[5]);

    /** Si le nombre de game ia vs ia est inférieur à 1 : placé à 1 par défaut. */
    if(occurencesNumber < 1)
        occurencesNumber = 1;

    /** Si le type d'IA du joueur 1 ne correspond, ni à rnd(Random), ni à mc (Monte Carlo), est placé à rnd par défaut.  */
    if(!((strcmp(player1Type, "rnd") == 0) || (strcmp(player1Type, "mc") == 0)))
        player1Type = strdup("rnd");

    /** Si le type d'IA du joueur 2 ne correspond, ni à rnd(Random), ni à mc (Monte Carlo), est placé à rnd par défaut.  */
    if(!((strcmp(player2Type, "rnd") == 0) || (strcmp(player2Type, "mc") == 0)))
        player2Type = strdup("rnd");

    /** Si le paramètre de l'IA 1 est random, mettre son paramètre auxiliaire à 1.*/
    if(strcmp(player1Type, "rnd") == 0)
        player1Parameter = 1;
    else
    {   /** Si le type de l'IA 1 est mc, il faut s'assurer que son paramètre auxiliaire est correct. */
        if(player1Parameter < 1)
            player1Parameter = 1;
    }

    /** Si le paramètre de l'IA 2 est random, mettre son paramètre auxiliaire à 1.*/
    if(strcmp(player2Type, "rnd") == 0)
        player2Parameter = 1;
    else
    {   /** Si le type de l'IA 2 est mc, il faut s'assurer que son paramètre auxiliaire est correct. */
        if(player2Parameter < 1)
            player2Parameter = 1;
    }

    /** Nombre de victoires pour l'IA 1. */
    int player1Victory = 0;

    /** Nombre de victoires pour l'IA 2. */
    int player2Victory = 0;

    /** Nombre de d'égalité. */
    int equality = 0;

    /** ID du premier joueur lors d'une partie IA vs IA, permet d'alterner. */
    int lastStartPlayerID = 1;

    /** Pour chaque parties IA vs IA.... */
    for(int i = 0 ; i  < occurencesNumber ; i++){
        lastStartPlayerID = currentPlayer->getId();
        while(!gameIsOver){/** Lancement de la boucle de jeu. */
            index = 0; /** Remise à zero de l'index de choix de placement. */
            choice.clear(); /** Remise à zero des choix de placements. */
            correctPlacements = board.checkCorrectPlacements(currentPlayer); /** Récupération des des placements possibles. */
            for(unsigned long int row = 0 ; row < correctPlacements.size() ; row++){ /** Pour chaque ligne du tableau de placement. */
                for(unsigned long int column = 0 ; column < correctPlacements[row].size() ; column++){ /** Pour chaque colonne du tableau de placement. */
                    if(correctPlacements[row][column] == 1){ /** Si l'IA a le droit de poser un pion... */
                        choice.push_back({row, column}); /** Ajout de ce choix à la liste des choix. */
                    }
                }
            }
            if(choice.size() > 0 && currentPlayer->getRemainingPieces() > 0){ /** Si l'IA possède encore des pions et qu'il peut en poser un... */
                currentPlayer->setCanPlay(true); /** l'IA est autorisé à jouer ce coup. */
                if(currentPlayer->getId() == 1){ /** Si l'IA actuel est l'IA 1... */
                    if(strcmp(player1Type, "rnd") == 0) /** Si son type est une IA random.... */
                        index = rand() % choice.size() + 0; /** Le choix de placement est aléatoire. */
                    else{ /** Sinon, l'IA est de type Monte Carlo... */
                        index = monteCarloAlgorithm(choice, player1Parameter, board, bot1, bot2); /** L'index est calculé via l'appel à l'algorithme de Monte Carlo. */
                    }
                }
                else
                {
                    if(currentPlayer->getId() == 2){ /** Si l'IA actuel est l'IA 2... */
                        if(strcmp(player2Type, "rnd") == 0) /** Si son type est une IA random.... */
                            index = rand() % choice.size() + 0; /** Le choix de placement est aléatoire. */
                        else{ /** Sinon, l'IA est de type Monte Carlo... */
                            index = monteCarloAlgorithm(choice, player2Parameter, board, bot2, bot1); /** L'index est calculé via l'appel à l'algorithme de Monte Carlo. */
                        }
                    }
                }
                board.placePiece(currentPlayer->getPiece(), choice[index][0] , choice[index][1]); /** Placement du pion.  */
                board.analyzeSurroundingPieces(choice[index][0], choice[index][1]); /** Retournement des pions alentours... */
            }else{ /** Si le joueur ne peut plus placer de pions... */
                currentPlayer->setCanPlay(false); /** Il passe son tour.... */
            }

            if(currentPlayer->getId() == 1){ /** Si l'IA actuel est l'IA n°1... */
                currentPlayer = &bot2; /** l'IA 2 joueura au prochain tour. */
            }
            else{ /** Si l'IA actuel est l'IA n°2... */
                currentPlayer = &bot1;/** l'IA 1 joueura au prochain tour. */
            }
            
            if((!bot1.getCanPlay() && !bot2.getCanPlay()) || bot1.getRemainingPieces() < 1 || bot2.getRemainingPieces() < 1){ /** Si aucun des joueurs ne peut jouer... */
                gameIsOver = true; /** La partie est terminée */
            }
        }

        /** Une fois la partie terminée. */
        bot1.setScore(board.getPiecesNumber(bot1.getColor())); /** Mise à jour du score de l'IA 1. */
        bot2.setScore(board.getPiecesNumber(bot2.getColor())); /** Mise à jour du score de l'IA 2. */
        if(bot1.getScore() > bot2.getScore()){ /** Si l'IA 1 a un score plus important que l'IA 2... */
            player1Victory++; /** On incrémente le nombre de victoires pour l'IA 1. */
        }else{ /** Si l'IA 2 a un score plus important que l'IA 1 ou qu'il y a égalité... */
            if(bot1.getScore() < bot2.getScore()){ /** Si l'IA 2 a un score plus important que l'IA 1... */
                player2Victory++; /** On incrémente le nombre de victoires pour l'IA 2. */
            }else{ /** Sinon ... */
                equality++; /** il y a égalité. */
            }
        }
        restartIAGame(); /** Relancement du jeu. */

        /** Avant le lancement d'une nouvelle partie IA vs IA, on alterne le joueur qui commencera la partie. */
        if(lastStartPlayerID == 1)
            currentPlayer = &bot2; /** Si l'IA 1 a lancée la partie précédente, l'IA 2 lancera la suivante. */
        else
            currentPlayer = &bot1; /** Si l'IA 2 a lancée la partie précédente, l'IA 1 lancera la suivante. */
    }

    /** Affichage du nombre de victoires pour chaque IA. */
    std::cout << occurencesNumber << ";" << player1Type << ";" << player1Parameter << ";" << player2Type << ";" << player2Parameter << ";" << player1Victory << ";" << player2Victory << ";" << equality << std::endl;
};

bool IA::rndGame(Board board, Player target, Player opponent){
    /** Le joueur qui doit jouer est celui passé en second paramètre de type joueur -> le joueur adverse....
     * En effet, lorsque cette méthode est apellée, le joueur "cible" vient tout juste de joueur son coup.
    */
    rndCurrentPlayer = &opponent;

    std::array<std::array<int, 8>, 8> tmpCorrectPlacements;
    for(auto& rows: tmpCorrectPlacements)
        for(auto &cell: rows)
            cell = 0;

    /** Création d'une grille de jeu temporaire. */
    Board tmpBoard = board;

    /** Booléen permettant de vérifier que la partie soit terminé. */
    bool rndGameIsOver = false;

    while(!rndGameIsOver){ /** Tant que la partie n'est pas terminé. */
        choice.clear(); /** on supprime le contenu du tableau de choix. */
        /** On récupère les placements de pions possibles. */
        tmpCorrectPlacements = tmpBoard.checkCorrectPlacements(rndCurrentPlayer);
        for(unsigned long int row = 0 ; row < tmpCorrectPlacements.size() ; row++){
            for(unsigned long int column = 0 ; column < tmpCorrectPlacements[row].size() ; column++){
                if(tmpCorrectPlacements[row][column] == 1){  /** Pour chaque placement possible...*/
                    choice.push_back({row, column}); /** On met à jour la liste des choix. */
                }
            }
        }

        /** si un choix est possible ... */
        if(choice.size() > 0 && rndCurrentPlayer->getRemainingPieces() > 0){
            rndCurrentPlayer->setCanPlay(true); /** l'IA est autorisé à jouer ce coup. */
            index = rand() % choice.size() + 0; /** Choix d'un placement aléatoire. */
            tmpBoard.placePiece(rndCurrentPlayer->getPiece(), choice[index][0] , choice[index][1]); /** placement d'un pion selon les coordonnées choisis. */
            tmpBoard.analyzeSurroundingPieces(choice[index][0], choice[index][1]); /** Retournement des pions alentours... */
        }else{
            rndCurrentPlayer->setCanPlay(false); /** L'IA passe son tour si elle ne peut pas jouer. */
        }
        if(rndCurrentPlayer->getId() == target.getId()){/** On change l'IA pour le prochain tour... */
            rndCurrentPlayer = &opponent;
        }
        else{
            rndCurrentPlayer = &target;
        }

        /** On vérifie que la partie ne soit pas terminé... */
        if((!target.getCanPlay() && !opponent.getCanPlay()) || target.getRemainingPieces() < 1 || opponent.getRemainingPieces() < 1){
            rndGameIsOver = true;
        }
    }
    target.setScore(tmpBoard.getPiecesNumber(target.getColor())); /** Mise à jour du score de l'IA 1. */
    opponent.setScore(tmpBoard.getPiecesNumber(opponent.getColor())); /** Mise à jour du score de l'IA 2. */

    /** Attention, 
     * On doit retourner true uniquement si le joueur ciblé vient de gagner la partie.
     */
    return target.getScore() > opponent.getScore();
};

int IA::monteCarloAlgorithm(std::vector<std::array<unsigned long int, 2>> choice, int occurrences, Board board, Player mainPlayer, Player opponentPlayer){
    /** Permet de gérer l'aléatoire. */
    srand((unsigned) time(0));
    Board monteCarloBoard; /** Création d'une grille de jeu temporaire. */
    std::vector<int> results; /** Liste regroupant le nombre de victoires allouées à chaque placement de pion. */
    std::pair<int, int> bestMove; /** Paire d'entier liant un coup spécifique à son score. la paire en question ne garde que le meilleur score. */
    for(unsigned long int i = 0; i < choice.size() ; i++){ /** Pour chaque choix à la disposition du joueur.... */
        monteCarloBoard = board; /** On initialise un tableau temporaire. */
        monteCarloBoard.placePiece(mainPlayer.getPiece(), choice[i][0] , choice[i][1]); /** On place le pion correspondant au choix. */
        monteCarloBoard.analyzeSurroundingPieces(choice[i][0], choice[i][1]); /** On retourne les pions alentours */
        results.push_back(0); /** On ajoute une valeur dans notre vecteur de résultats. */
        for(int j = 0 ; j < occurrences ; j++){ /** Pour chaque occurence -> paramètre de l'algorithme de Monte Carlo.... */
            /** On crée une partie en se basant sur l'état du tableau après le coup initial
             * On pense également à "inverse" l'ordre des joueurs en paramètre, le "mainPlayer" vient de placer son coup, 
             * Le prochain joueur à jouer est donc le joueur adverse...
             */
            if(rndGame(monteCarloBoard, mainPlayer, opponentPlayer))
                results[i]++; /** Si la partie se termine et que le joueur principal a gagné.... */
        }
    }

    bestMove = std::make_pair(0, 0); /** Création d'une paîre vide, préparation pour trouver le plus grand nombre de victoire, et le choix lié. */
    for(unsigned long int i = 0 ; i < results.size() ; i++){ /** Pour chaque valeur présent dans la liste de résultats... */
        if(results[i] > bestMove.second){ /** Si le nombre de victoire en question est plus grand que le nombre de victoire max... */
            bestMove.first = i; /** On précise que le nombre de victoire max appartient à l'index de choix en question */
            bestMove.second = results[i]; /** On met à jour le nombre de victoire max... */
        }
    }
    return bestMove.first; /** On retourne l'index ayant entrainé le plus de victoire... */
};

void IA::restartIAGame(){
    board.initBoard(); /** re-initialisation de la grille de jeu */
    bot1.initPlayer(); /** re-initialisation du joueur 1 */
    bot2.initPlayer(); /** re-initialisation du joueur 2 */
    currentPlayer = &bot1;
    gameIsOver = false;
}

int IA::callMonteCarloAlgorithm(std::vector<std::array<unsigned long int, 2>> choice, Board board, Player mainPlayer, Player opponentPlayer){
    return monteCarloAlgorithm(choice, MONTECARLO_DEFAULT, board, mainPlayer, opponentPlayer);
}