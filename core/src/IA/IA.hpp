#pragma once
#include "../player/Player.hpp"
#include "../board/Board.hpp"
#include <cstdlib> 
#include <array>
#include <utility>
#include <string>
#include <cstring>

class IA{
    
        /**
         * Quelques attributs privés ont été rajoutés ici dans le but de rendre cette classe un peu plus indépendante de la classe Game.
         * 
         * La relation IA - Game est assez complexe.
         * 
         * Game a besoin d'IA pour faire appel à l'algorithme de Monte Carlo.
         * Mais IA doit être capable de lancer des parties de jeu, tout comme Game.
         * 
         * ils doivent également garder un minimum d'indépendance pour fonctionner correctement.
         * 
         * Ainsi, IA n'herite plus de Game car la classe Game doit désormais appeler l'IA lors d'un coup.
         * 
         * En conséquence, on ne peut plus utiliser les attributs de Game dans notre classe IA directement.
         * 
         * Ainsi, le diagramme UML est également un peu plus complexe.
         * Désormais, nos classes Player et Board possèdent des instances dans deux classes différentes -> IA ET GAME.
         * 
         * J'essayerai d'optimiser cette relation en rendant le diagramme également moins complexe.
         **/
    private:

        /** @brief Index utilisé pour gérer le prochain coup d'un joueur. */
        unsigned long int index = 0;

        /** @brief Pointeur vers un joueur, utilisé par la comparaison d'IA pour créer ses parties. */
        Player * currentPlayer;

        /** @brief Pointeur vers un joueur, utilisé par rndGame pour créer ses parties. */
        Player * rndCurrentPlayer;

        /** @brief Créations de joueurs. */
        Player bot1, bot2;

        /** @brief Grille de jeu. */
        Board board;

        /** @brief Liste de choix. */
        std::vector<std::array<unsigned long int, 2>> choice;

        /** @brief Grille de placements possibles. */
        std::array<std::array<int, 8>, 8> correctPlacements;

        /** @brief booléen de partie fini. */
        bool gameIsOver = false;
        /** 
         * @brief Méthode permettant de créer une partie totalement basée sur des actions random.
         * Cette fonction va être utilisée par l'algorithme de Monte Carlo.
         * 
         * @param board La grille de jeu donné à l'instant t, la partie se déroulera à partir de cette grille.
         * @param target Joueur actuel concerné lors de l'appel à l'algorithme de Monte Carlo.
         * @param opponent Joueur adverse concerné lors de l'appel à l'algorithme de Monte Carlo.
         * 
         * @return true si la cible gagne, false sinon.
         */
        bool rndGame(Board board, Player target, Player opponent);

        /**
         * @brief Algorithme de Monte Carlo.
         * 
         * Explication du déroulement de cet algorithme :
         * 
         * L'algorithme de Monte Carlo a pour but de fournir un placement de pion idéal à l'instant t du jeu.
         * Pour trouver le meilleur placement possible,
         * On lui fourni la liste des placements valide à l'instant t.
         * 
         * Pour chaque placement valide, 
         * il va tout d'abord jouer le coup sur une table de jeu temporaire.
         * Ensuite, il va faire appel à la méthode rndGame() pour vérifier l'issue de la partie après le coup joué.
         * 
         * Afin d'ajuster la précision de cet algorithme,
         * La partie sera créée de multiples fois (via des appels multiples à rndGame()).
         * 
         * rndGame renvoi true si le joueur principal gagne.
         * Nous allons donc utiliser cela pour vérifier combien de fois le joueur concerné gagne, et cela pour chaque coup à sa disposition.
         * 
         * Finalement, le coup gardé en mémoire (et retourné par cette méthode) est le coup lié au plus grand nombre de victoires du joueur.
         * 
         * @param choice Liste des choix à la disposition du joueur lors de l'appel à l'algorithme
         * @param occurences Nombre de parties aléatoires que l'algorithme génére pour chaque choix de coup.
         * @param board La grille de jeu, correspond à l'état actuel de la partie lors de l'appel.
         * @param mainPlayer Joueur qui placera son pion selon le retour de l'algorithme.
         * @param opponentPlayer Joueur adverse
         * 
         * @return L'index de la liste de choix lié au plus grand nombre de victoire.
         */
        int monteCarloAlgorithm(std::vector<std::array<unsigned long int, 2>> choice, int occurrences, Board board, Player mainPlayer, Player opponentPlayer);

    public:
        /** @brief Constructeur */
        IA();
        ~IA();

        /** 
         * @brief Méthode permettant de réaliser des duels IA vs IA.
         * 
         * @param argc Nombre de paramètres donnés lors de l'appel à ce programme.
         * @param argv Chaine de caractère incluant les paramètres donnés lors de l'appel à ce programme
         */
        void startGame(int argc, char *argv[]);

        /** @brief Méthode permettant de relancer une partie temporaire pour les IA. */
        void restartIAGame();

        /**
         * @brief Méthode permettant d'apeller l'algorithme de Carlo depuis la classe Game.
         * Cette méthode apelle simplement la méthode privée monteCarloAlgorithm() en fournissant le nombre d'occurences par défaut (voir variable globale dans IA.cpp).
         * 
         * @param choice Liste des choix à la disposition du joueur lors de l'appel à l'algorithme
         * @param board La grille de jeu, correspond à l'état actuel de la partie lors de l'appel.
         * @param mainPlayer Joueur qui placera son pion selon le retour de l'algorithme.
         * @param opponentPlayer Joueur adverse
         * 
         * @return L'index de la liste de choix lié au plus grand nombre de victoire.
         */
        int callMonteCarloAlgorithm(std::vector<std::array<unsigned long int, 2>> choice, Board board, Player mainPlayer, Player opponentPlayer);
};