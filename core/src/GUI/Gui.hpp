#pragma once

#include "../game/Game.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <cstdlib> 
#include <array>
#include <utility>
#include <string>

class GUI : public Game{
    
    private:
        /** @brief int : Largeur de la fenêtre */
        int WIDTHSCREEN;

        /** @brief int : Hauteur de la fenêtre */
        int HEIGHTSCREEN;

        /** 
         * @brief Booléen permettant de lancer une boucle de jeu,
         * Tant que cette valeur est à true, la fenêtre reste active.
         * 
         * Dès qu'elle est à false, la fenêtre est détruite.
        */
        bool isRunning = true;

        /** @brief Pointeur vers une variable SDL_Window pour gérer la fenêtre. */
        SDL_Window* pWindow{ nullptr };

        /** @brief Pointeur vers une variable SDL_Renderer pour gérer le rendu. */
        SDL_Renderer* pRenderer{ nullptr };

        /** @brief Pointeur vers une variable SDL_Event permettant de vérifier si l'utilisateur effectue une action. */
        SDL_Event event;

        /** @brief Pointeur vers une variable TTF_Font permettant d'importer une police d'écriture. */
        TTF_Font* font;

        /** @brief Couleur par défaut d'un pion blanc. */
        SDL_Color whiteColor{255, 255, 255, 255};

        /** @brief Couleur par défaut d'un pion noir. */
        SDL_Color blackColor{0, 0, 0, 255};

        /** @brief Couleur par défaut d'un placement de pion possible pour le joueur. */
        SDL_Color placementColor{0, 191, 255, 255};

        /** @brief Point SDl utilisé pour contenir des coordonnées (souris). */
        SDL_Point point;

        /** @brief Rectangle permettant de définir une position de texte. */
        SDL_Rect position;

        /** @brief Pointeur vers une surface de texte. */
        SDL_Surface* text;

        /** @brief Pointeur vers une texture de texte. */
        SDL_Texture* texture;

        /** @brief Tableau 2 Dimensions contenant des coordonnées pour les pions. **/
        std::array<std::array<std::pair<int, int>, 8>, 8> piecesCoordinates;

        /** @brief Tableau contenant uniquement des rectangles, utilisé pour détecter facilement les clicks de souris. **/
        std::array<std::array<SDL_Rect, 8>, 8> piecesDetections;

        /** @brief Méthode privée permettant de créer une grille rapidement. **/
        void createGrid();

        /** @brief Méthode privée permettant de changer de joueurs et de mettre à jour l'affichage en conséquence. **/
        void changePlayerAndUpdate();

        /** @brief Méthode privée permettant d'afficher les informations de joueurs : nom, score, pions restants. **/
        void printPlayersInformation();

        /** @brief Méthode privée permettant d'afficher le nom du joueur actuel. **/
        void printCurrentPlayerName();

        /** 
         * @brief Méthode privée permettant de créer et d'afficher un texte dans la fenêtre.
         * 
         * @param texte : Le texte à afficher.
         * @param xPosition : Position en x du texte à créer.
         * @param yPosition : Position en y du texte à créer.
         * @param fontSIze : Taille de la police d'écriture à utiliser.
         **/
        void createText(const char * texte, int xPosition, int yPosition, int fontSize);

        /** @brief Méthode privée permettant de gérer un click souris gauche. **/
        void handleLeftClick(int xPosition, int yPosition);

        /** @brief Méthode privée permettant de gérer un click souris Millieu. **/
        void handleMiddleClick();

        /** @brief Méthode privée permettant de gérer un click souris Droit. **/
        void handleRightClick();

        /** @brief Méthode privée permettant de gérer la fin de partie. **/
        void handleEndGame();

        /** @brief Méthode privée permettant de créer le texte d'affichage pour la fin de partie. **/
        void createWinnerText();

        /** 
         * @brief Méthode privée permettant d'afficher la grille de jeu, sans les placements de pions.
         * Très utile pour l'écran de fin de partie.
         **/
        void printBoard();

        /** @brief Méthode privée permettant d'afficher un message informant l'utilisateur que le jeu va prochainement redémarrer. **/
        void createReloadGameText();

        /** @brief Méthode privée permettant de mettre à jour la grille de jeu. **/
        void updateGameBoard();

        /** @brief Méthode privée permettant de mettre à jour la fenêtre, utile par exemple, lors d'un changement de joueur. **/
        void updateWindow();

        /** 
         * @brief Méthode privée permettant de jouer un coup directement, la méthode se charge du nécéssaire ensuite : 
         * Placement de pion, analyse de la grille, changement de joueur, etc....
         * 
         * @param i : la coordonnée x du pion à poser.
         * @param j : la coordonnée y du pion à poser.
         **/
        void playMove(unsigned long int i, unsigned long int j);

    public:
        /** @brief Cosntructeur **/
        GUI();

        /** @brief Destructeur **/
        ~GUI();

        /** @brief Méthode permettant de vérifier les différentes actions possibles en cours de partie. **/
        void checkEvents();

         /**@brief  Méthode permettant de redémarrer le jeu avec une actualisation de la fenêtre. **/
        void reloadGameAndWindow();

        /**
         * @brief Getter permettant de récupèrer la valeur de isRunning.
         * 
         * @return La valeur de isRunning.
         **/
        bool getIsRunning();

        /**
         * @brief Setter permettant de modifier la valeur de isRunning.
         * 
         * @param _isRunning La nouvelle valeur de isRunning.
         **/
        void setIsRunning(bool _isRunning);
};