#include "Gui.hpp"
#include <iostream>
#include <vector>
#include <string>
#include <sstream>

GUI::GUI() : Game(){
    WIDTHSCREEN = 800; /** initialisation de la taille de fenetre par défaut. */
    HEIGHTSCREEN = 600;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) { /** initialisation de SDL2. */
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "[DEBUG] > %s", SDL_GetError());
        SDL_Quit();
    }

    if (TTF_Init() < 0) /** initialisation de TTF(SDL2) : gestion de texte. */
    {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "[DEBUG] > %s", TTF_GetError());
        SDL_Quit();
    }

    if (SDL_CreateWindowAndRenderer(WIDTHSCREEN, HEIGHTSCREEN, SDL_WINDOW_SHOWN, &pWindow, &pRenderer) < 0) /** Création de la fenetre et du rendu. */
    {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "[DEBUG] > %s", SDL_GetError());
        SDL_Quit();
    }

    SDL_SetWindowTitle(pWindow, "REVERSI"); /** Titre de la fenetre. */
    SDL_SetRenderDrawColor(pRenderer, 30, 100, 50, 255); /** Couleur par défaut de la fenetre. */
    SDL_RenderClear(pRenderer); /** Remplissage de la fenetre de rendu avec la couleur par défaut. */

    point = SDL_Point{0, 0}; /** Point permettant de contenir les coordonnées de click-souris. */

    for(int i = 0 ; i < 8 ; i++){
        for(int j = 0 ; j < 8 ; j++){
            piecesCoordinates[i][j] = std::make_pair(i * 50 + 4 * 50, j * 50 + 2 * 50); /** Création d'une grille de jeu : Initialisation des coordonnées. */
        }
    }
    createGrid(); /** Appel à la création d'une grille de jeu. */
    updateWindow(); /** Mise à jour de la fenetre. */
};

GUI::~GUI(){ /** Destructeur */
    SDL_DestroyRenderer(pRenderer); /** Destruction du pointeur rendu. */
    SDL_DestroyWindow(pWindow); /** Destruction du pointeur fenetre .*/
    TTF_Quit(); /** On cesse d'utiliser SDL_TTF. */
    SDL_Quit(); /** On cesse d'utiliser SDL2. */
};

void GUI::createGrid(){
    SDL_SetRenderDrawColor(pRenderer, 0, 0, 0, 255); /** Couleur pour la grille de jeu */
    for(int i = 0 ; i < 8 ; i++){
        for(int j = 0 ; j < 8 ; j++){
            const SDL_Rect * rect = new SDL_Rect{piecesCoordinates[i][j].first, piecesCoordinates[i][j].second, 50, 50};
            piecesDetections[i][j] = *rect; /** On récupère le rectangle pour le stocker à part. */
            SDL_RenderDrawRect(pRenderer, rect); /** Création de la grille, chaque rectangle correspond à un pointeur...*/
            delete rect; /** mais ce dernier est ensuite supprimé. */
        }
    }
};

void GUI::updateGameBoard(){ /** Mise à jour de la grille de jeu. */
    correctPlacements = board.checkCorrectPlacements(currentPlayer); /** Récupèration des placements de pions pour le joueur. */
    choice.clear();
    for(unsigned long int i = 0 ; i < 8 ; i++){
        for(unsigned long int j = 0 ; j < 8 ; j++){ /** Pour chaque case de la grille de jeu.... */
            if(board.getBoardCell(i, j) == 'W')
                filledCircleRGBA(pRenderer, piecesCoordinates[i][j].first + 25, piecesCoordinates[i][j].second+25, 20, whiteColor.r, whiteColor.g, whiteColor.b, whiteColor.a); /** Création d'un pion blanc. */
            else
                if(board.getBoardCell(i, j) == 'B')
                    filledCircleRGBA(pRenderer, piecesCoordinates[i][j].first + 25, piecesCoordinates[i][j].second+25, 20, blackColor.r, blackColor.g, blackColor.b, blackColor.a); /** Création d'un pion noir. */
            if(correctPlacements[i][j] == 1){
                filledCircleRGBA(pRenderer, piecesCoordinates[i][j].first + 25, piecesCoordinates[i][j].second+25, 10, placementColor.r, placementColor.g, placementColor.b, placementColor.a); /** Création d'un placement de pion éventuel. */
                choice.push_back({i, j});
            }
        }
    }
    if(!currentPlayer->getCanPlay()) /** Si le joueur ne peut pas jouer, il passe son tour directement. */
        changePlayerAndUpdate();
};

void GUI::checkEvents(void){
    /** Est apellée en boucle.  */
    SDL_RenderPresent(pRenderer); /** Mise à jour de la fenetre. */
    while(SDL_PollEvent(&event)){ /** Tant qu'un événement a été détecté ... -> si le joueur ne fait rien, on entre pas dans cette boucle. */
        switch(event.type){ /** Selon le type d'evenement */
            case SDL_QUIT:
                isRunning = false; /** On précise ici que l'utilisateur souhaite quitter l'application (ne fonctionne que pour la gui local, en web, il est nécéssaire de fermer l'onglet. ) */
                break;
            case SDL_MOUSEBUTTONUP: /** Si l'utilisateur a effectué un click de souris ... */
                if (event.button.button == SDL_BUTTON_LEFT)/** Click de souris gauche. */
                    handleLeftClick(event.button.x, event.button.y); /** On récupère les coordonnées de click. */

                if (event.button.button == SDL_BUTTON_MIDDLE)/** Click de souris Milieu. */
                    handleMiddleClick();

                if (event.button.button == SDL_BUTTON_RIGHT)/** Click de souris Droit. */
                    handleRightClick();
                break;
        }
    }
};

void GUI::printCurrentPlayerName(){
    createText(currentPlayer->getName().c_str(), WIDTHSCREEN / 2, 10, 20); /** Affichage du nom du joueur actuel. */
};

void GUI::printPlayersInformation(){
    player1.setScore(board.getPiecesNumber(player1.getColor())); /** Mise à jour des scores. */
    player2.setScore(board.getPiecesNumber(player2.getColor()));

    /** Affichage des informations joueurs. */
    std::string ss1 = player1.getName() + " -- Score : " + std::to_string(player1.getScore()) + " (" + std::to_string(player1.getRemainingPieces()) + " pions restants).";
    std::string ss2 = player2.getName() + " -- Score : " + std::to_string(player2.getScore()) + " (" + std::to_string(player2.getRemainingPieces()) + " pions restants).";

    createText(ss1.c_str(), WIDTHSCREEN / 2, HEIGHTSCREEN - 60, 20);
    createText(ss2.c_str(), WIDTHSCREEN / 2, HEIGHTSCREEN - 30, 20);
};

void GUI::createText(const char * texte, int xPosition, int yPosition, int fontSize){
    font = TTF_OpenFont("assets/roboto.ttf", fontSize); /** ouverture de la police d'écriture. */
    if (font == nullptr)
        std::cout << TTF_GetError() << std::endl;

    text = TTF_RenderUTF8_Blended(font, texte, whiteColor); /** Création d'une surface de rendu texte. */
    if (text == nullptr)
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "[DEBUG] > %s", TTF_GetError());

    texture = SDL_CreateTextureFromSurface(pRenderer, text); /** Création d'une texture pour la surface texte. */
    if (texture == nullptr)
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "[DEBUG] > %s", TTF_GetError());

    SDL_QueryTexture(texture, nullptr, nullptr, &position.w, &position.h); /** Récupèration de la texture et application d'une hauteur et largeur. */
    position.x = xPosition - position.w / 2;
    position.y = yPosition;
    SDL_RenderCopy(pRenderer, texture, nullptr, &position); /** Plaquage de la texture dans la fenetre aux coordonnées indiqués. */

    SDL_DestroyTexture(texture); /** Destruction de la texture. */
    SDL_FreeSurface(text); /** Destruction de la surface. */
    TTF_CloseFont(font); /** Destruction de la police d'écriture. */
};

void GUI::updateWindow(){
    /** Reinitialisation du contenu de la fenetre. */
    SDL_SetRenderDrawColor(pRenderer, 30, 100, 50, 255);
    SDL_RenderClear(pRenderer);
    createGrid(); /** Création d'une grille de jeu. */
    printCurrentPlayerName(); /** Affichage du joueur courant. */
    printPlayersInformation(); /** Affichage des infos joueurs. */
    updateGameBoard(); /** Mise à jour de la grille de jeu. */
    SDL_RenderPresent(pRenderer); /** Mise à jour de la fenetre. */
};

void GUI::handleLeftClick(int xPosition, int yPosition){
    for(int i = 0 ; i < 8 ; i++){
        for(int j = 0 ; j < 8 ; j++){ /** Pour chaque case de la grille de jeu... */
            point.x = xPosition; /** Placement d'un point aux coordonnées de click souris. */
            point.y = yPosition;
            if(SDL_PointInRect(&point, &piecesDetections[i][j])){ /** Vérification : la case sélectionné contient-elle le point ? */
                if(correctPlacements[i][j] == 1){  /** Si la case sélectionné contient ce point, on vérifie que le joueur possède l'autorisation de placer un pion.... */
                    playMove(i, j); /** Le joueur place le pion. */
                }
            }
        }
    }
};

void GUI::handleMiddleClick(){
    /** Rechargement du jeu complet. */
    reloadGameAndWindow();
};

void GUI::handleRightClick(){
    isRunning = false; /** Fermeture de l'application manuel, ne fonctionne qu'avec le programme gui local (inutile en web) */
};

void GUI::changePlayerAndUpdate(){
    /** On vérifie que nous ne sommes pas en situation de fin de jeu... */
    if((!player1.getCanPlay() && !player2.getCanPlay()) || player1.getRemainingPieces() < 1 || player2.getRemainingPieces() < 1){
        handleEndGame(); /** Appel à la fin de jeu. */
    }
    else{
        if(currentPlayer->getId() == 1){ /** Changement de joueur. */
            currentPlayer = &player2;
        }
        else{
            currentPlayer = &player1;
        }
        updateWindow(); /** Mise à jour de la fenetre de jeu avec maj des placements pour le nouveau joueur. */
        SDL_RenderPresent(pRenderer); /** Mise à jour du rendu. */

        if(currentPlayer->getId() == 2){ /** Si le joueur est l'IA... */
            index = IABot.callMonteCarloAlgorithm(choice, board, player2, player1); /** Appel à l'algorithme de Monte Carlo pour laisser l'IA jouer. */
            playMove(choice[index][0], choice[index][1]); /** L'IA joue son coup directement. */
            /** PlayMove se charge également de faire appel à cette même méthode changePlayerAndUpdate().*/
        }
    }
    SDL_RenderPresent(pRenderer); /** Mise à jour du rendu. */
}

void GUI::handleEndGame(){
    /* On précise que la partie est terminée. */
    gameIsOver = true;
    SDL_SetRenderDrawColor(pRenderer, 57, 57, 58, 255); /** Fond gris */
    SDL_RenderClear(pRenderer); /** Mise à jour du fond de fenetre. */
    createGrid(); /** Création de la grille de jeu */
    printBoard(); /** Récupèration et affichage des pions à la fin de jeu. */
    createWinnerText(); /** On affiche le gagnant. */
    createReloadGameText(); /** On affiche le texte de rechargement de partie. */
    SDL_RenderPresent(pRenderer); /** Mise à jour du rendu. */
}

void GUI::createWinnerText(){
    player1.setScore(board.getPiecesNumber(player1.getColor())); /** Mise à jour des scores. */
    player2.setScore(board.getPiecesNumber(player2.getColor()));

    if(player1.getScore() > player2.getScore()){ /** Si le joueur 1 gagne... */
        std::string ss = player1.getName() + " gagne avec " + std::to_string(player1.getScore()) + " points contre " + std::to_string(player2.getScore()) + ".";
        createText(ss.c_str(), WIDTHSCREEN / 2, 10, 20); /** Affichage du texte. */
    }else{
        if(player1.getScore() < player2.getScore()){ /** Si le joueur 2 gagne... */
            std::string ss = player2.getName() + " gagne avec " + std::to_string(player2.getScore()) + " points contre " + std::to_string(player1.getScore()) + ".";
            createText(ss.c_str(), WIDTHSCREEN / 2, 10, 20); /** Affichage du texte. */
        }else{ /** Si égalité */
            std::string ss = "Les deux joueurs sont à égalité avec " + std::to_string(player1.getScore()) + " points.";
            createText(ss.c_str(), WIDTHSCREEN / 2, 10, 20); /** Affichage du texte. */
        }
    }
};

void GUI::printBoard(){
    for(int i = 0 ; i < 8 ; i++){
        for(int j = 0 ; j < 8 ; j++){ /** Pour chaque case du tableau... */
            if(board.getBoardCell(i, j) == 'W')
                filledCircleRGBA(pRenderer, piecesCoordinates[i][j].first + 25, piecesCoordinates[i][j].second+25, 20, whiteColor.r, whiteColor.g, whiteColor.b, whiteColor.a); /** Affichage des pions blancs. */
            else
                if(board.getBoardCell(i, j) == 'B')
                    filledCircleRGBA(pRenderer, piecesCoordinates[i][j].first + 25, piecesCoordinates[i][j].second+25, 20, blackColor.r, blackColor.g, blackColor.b, blackColor.a); /** Affichage des pions noirs. */
        }
    }
};

void GUI::createReloadGameText(){
    createText("Le jeu va redémarrer automatiquement dans quatre secondes...", WIDTHSCREEN / 2, HEIGHTSCREEN - 60, 28);
};

void GUI::reloadGameAndWindow(){
    restartGame(); /** Rechargement du jeu : état, variables, etc... */
    updateWindow(); /** Mise à jour de la fenetre. */
};

void GUI::setIsRunning(bool _isRunning){
    isRunning = _isRunning;
}

bool GUI::getIsRunning(){
    return isRunning;
};

void GUI::playMove(unsigned long int i, unsigned long int j){
    board.placePiece(currentPlayer->getPiece(), i, j); /** placement du pion par le joueur. */
    board.analyzeSurroundingPieces(i, j); /** Analyse et retournement des pions alentours. */
    changePlayerAndUpdate(); /** Changement du joueur et mise à jour. */
}