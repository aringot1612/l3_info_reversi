#include "GUI/Gui.hpp"

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#include <emscripten/bind.h>
#endif

GUI gui = GUI(); /** Instance de jeu graphique. */

void gameLoop() /**Boucle de jeu graphique. */
{
  if(!gui.getGameIsOver()) /** Si la partie n'est pas terminée... */
    gui.checkEvents(); /** On vérifie les input utilisateur.  */
  else{ /** Si la partie est terminée... */
    SDL_Delay(4000); /** On attend 4 secondes avant de relancer le jeu... */
    gui.reloadGameAndWindow();
  }
}

int main(){
  /** Si emscripten est utilisé lors de la compilation, on utilise une boucle emscripten pour le web -> fréquence de la boucle = fréquence du navigateur. */
  #ifdef __EMSCRIPTEN__
    emscripten_set_main_loop(gameLoop, 0, 1);
  #else /** Si emscripten n'est pas utilisé lors de la compilation, on crée notre propre boucle. */
    while (gui.getIsRunning()) {
      gameLoop();
    }
  #endif
};