#include "Game.hpp"
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <cstdlib>

Game::Game() : board(), gameIsOver(false){
    player1 = Player("Joueur 1 (Couleur de pion : Noir)", 'B', 1); /** Création des joueurs par défaut. */
    player2 = Player("Joueur 2 (Couleur de pion : Blanc)", 'W', 2);
    currentPlayer = &player1; /** Le joueur 1 joue en premier. */
    gameIsOver = false;
};

Game::~Game(){}

void Game::printBoard(){
    std::cout << board << std::endl; /** Affichage de la grille de jeu console. */
};

void Game::play(){
    std::string answer; /** Permet de stocker le choix du joueur. */
    while(!gameIsOver){ /** Tant que la partie n'est pas terminée... */
        player1.setScore(board.getPiecesNumber(player1.getColor())); /** Mise à jour des scores. */
        player2.setScore(board.getPiecesNumber(player2.getColor()));

        std::cout << "Score actuel du joueur 1 : " << player1.getScore() << " points." << std::endl; /** Affichage des scores. */
        std::cout << "Score actuel du joueur 2 : " << player2.getScore() << " points." << std::endl;
        std::cout << std::endl;
        correctPlacements = board.checkCorrectPlacements(currentPlayer); /** Récupèration des coups jouables. */
        index = 0;
        choice.clear(); /** Effacement de la liste de choix */
        for(unsigned long int row = 0 ; row < correctPlacements.size() ; row++){
            for(unsigned long int column = 0 ; column < correctPlacements[row].size() ; column++){ /** On parcourt le mapping des placements... */
                if(correctPlacements[row][column] == 1){ /** Si le placement de pion est disponible... */
                    choice.push_back({row, column}); /** Ajout de ce placement à la liste des choix. */
                }
            }
        }

        if(choice.size() > 0 && currentPlayer->getRemainingPieces() > 0){ /** Si le joueur courant possède encore des pions et qu'il est capable de placer un pion */
            currentPlayer->setCanPlay(true); /** On précise qu'il est autorisé à jouer. */
            printBoard(); /** Affichage de la grille de jeu. */
            std::cout << std::endl;
            std::cout << "Le joueur "<< currentPlayer->getId() << " joue." << std::endl;
            std::cout << "Il possède encore "<< currentPlayer->getRemainingPieces() << " pions." << std::endl;
            std::cout << std::endl;
            std::cout << "Choix des coordonnées :" <<std::endl;
            
            for (unsigned long int i = 0 ; i < choice.size() ; i++) {
                std::cout << "Choix " << i + 1 << " | Coordonnées : " << choice[i][0] + 1 << "---" << choice[i][1] + 1 << ".\n"; /** Listing des choix à sa disposition. */
            }
            std::cout << "Entrez l'index correspondant aux coordonnées de placement de votre pion." << std::endl;
            if(currentPlayer->getId()==2){
                index = IABot.callMonteCarloAlgorithm(choice, board, player2, player1);
            }
            else{
                std::cin >> index;
                index--;
            }
            while(!(index <= choice.size())){ /** tant que son choix est invalide (ne correspond pas à la liste des choix disponibles....) */
                std::cout << "Veuillez réessayer, valeur incorrect." << std::endl;
                std::cin >> index; /** On le relance. */
            }
            /** Son choix est confirmé arrivé ici. */
            

            board.placePiece(currentPlayer->getPiece(), choice[index][0] , choice[index][1]); /** Placement du pion à l'endroit indiqué. */
            board.analyzeSurroundingPieces(choice[index][0], choice[index][1]); /** Analyse et retournement des pions alentours suite au placement. */
            
        }else{ /** Si le joueur est dans l'incapacité de jouer ce coup... */
            std::cout << std::endl;
            std::cout << "Le joueur "<< currentPlayer->getId() << " passe son tour." << std::endl;
            currentPlayer->setCanPlay(false); /** On met à jour son état. */
        }

        if(currentPlayer->getId() == 1){ /** Changement du joueur. */
            currentPlayer = &player2;
        } 
        else{
            currentPlayer = &player1;
        } 

        if((!player1.getCanPlay() && !player2.getCanPlay()) || player1.getRemainingPieces() < 1 || player2.getRemainingPieces() < 1){ /** Si la partie est terminée.... */
            gameIsOver = true; /** On met à jour le booléen concerné, ce qui a pour effet de quitter la boucle de jeu. */
        }
    }
    system("clear");
    printBoard();
    std::cout << "La parti est terminé !!." << std::endl;
    std::cout << std::endl;
    player1.setScore(board.getPiecesNumber(player1.getColor())); /** Mise à jour des scores. */
    player2.setScore(board.getPiecesNumber(player2.getColor()));

    if(player1.getScore() > player2.getScore()){ /** Si le joueur 1 gagne... */
        std::cout << "Le joueur 1 gagne avec " << player1.getScore() << " points contre " << player2.getScore() << "." << std::endl;
    }else{
        if(player1.getScore() < player2.getScore()){ /** Si le joueur 2 gagne... */
            std::cout << "Le joueur 2 gagne avec " << player2.getScore() << " points contre " << player1.getScore() << " ." << std::endl;
        }else{ /** Si il y a égalité. */
            std::cout << "Les deux joueurs sont à égalité avec " << player1.getScore() << " points." << std::endl;
        }
    }
};

void Game::restartGame(){ /** Rechargement du jeu. */
    board.initBoard(); /** re-initialisation de la grille de jeu */
    player1.initPlayer(); /** re-initialisation du joueur 1 */
    player2.initPlayer(); /** re-initialisation du joueur 2 */
    currentPlayer = &player1; /** Joueur par défaut */
    gameIsOver = false; /** La partie recommence */
};

bool Game::getGameIsOver(){
    return gameIsOver;
}

void Game::setGameIsOver(bool _gameIsOver){
    gameIsOver = _gameIsOver;
}