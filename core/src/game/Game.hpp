#pragma once
#include "../board/Board.hpp"
#include "../player/Player.hpp"
#include "../IA/IA.hpp"

class Game{
    protected:
        /** @brief Instance d'une IA permettant d'utiliser des bots en jeu. */
        IA IABot;

        /** @brief Instance de la classe Board */
        Board board;

        /** @brief Instance de la classe Joueur : Joueur 1 */
        Player player1;

        /** @brief Instance de la classe Joueur : Joueur 2 */
        Player player2;

        /** @brief Pointeur vers l'un des joueurs. */
        Player* currentPlayer;

        /** @brief booléen renseignant si la partie est terminée ou non. */
        bool gameIsOver;

        /** @brief Tableau contenant les placements de pions possibles pour le joueur courant. */
        std::array<std::array<int, 8>, 8> correctPlacements;

        /** @brief Liste de choix joueurs pour les placements. */
        std::vector<std::array<unsigned long int, 2>> choice;

        /** @brief Index de tableau pour des choix de placements. */
        unsigned long int index = 0;

    public:
        /** @brief Constructeur */
        Game();

        /** @brief Destructeur */
        ~Game();

        /** @brief Affiche le contenu de notre grille de jeu via son l'operateur surchargé (<<). */
        void printBoard();

        /** @brief Méthode exécutant en boucle un système tour par tour pour deux joueurs, avec placement de pion. */
        void play();

        /** @brief Méthode permettant d'initialiser la grille de jeu selon les règles du Reversi. */
        void startGame();

        /** 
         * @brief Méthode permettant de relancer une partie en effectuant les opérations de réinitilisations nécéssaires.
         * Pour le moment, seule l'interface GUI bénéficie de cette fonctionnalité.
         **/
        void restartGame();

        /** 
         * @brief Getter permettant de récupèrer la valeur de gameIsOver.
         * 
         * @return la valeur de gameIsOver.
         */
        bool getGameIsOver();

        /** 
         * @brief Setter permettant de modifier la valeur de gameIsOver.
         * 
         * @param _gameIsOver la nouvelle valeur de gameIsOver.
         */
        void setGameIsOver(bool _gameIsOver);
};