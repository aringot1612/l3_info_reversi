#include "Piece.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupPiece) { };

/** 
 * @brief Vérification du constructeur par défaut d'un pion.
 * 
 * Un pion est crée, on vérifie sa "couleur" à l'initialisation par défaut.
 */
TEST(GroupPiece, TestPiece_1)
{
    Piece piece;
    CHECK_EQUAL(piece.getColor(), '.');
};

/** 
 * @brief Vérification du constructeur d'un pion avec couleur.
 * 
 * Un pion est crée avec une couleur définie, on vérifie sa "couleur" par la suite.
 */
TEST(GroupPiece, TestPiece_2)
{
    Piece piece('B');
    CHECK_EQUAL(piece.getColor(), 'B');
};

/** 
 * @brief Vérification du constructeur d'un pion avec couleur et retournement de pion
 * 
 * Un pion est crée avec une couleur définie, on le retourne.
 * Et on vérifie sa "couleur" par la suite.
 */
TEST(GroupPiece, TestPiece_3)
{
    Piece piece('B');
    CHECK_EQUAL(piece.getColor(), 'B');
    piece.changeColor();
    CHECK_EQUAL(piece.getColor(), 'W');
};