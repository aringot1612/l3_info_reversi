#pragma once

class Piece{
    
    private:

        /** @brief Caractère représentant une couleur (B pour black/noir | W pour white/blanc). */
        char color;

    public:

        /** @brief Constructeur par défaut. */
        Piece();

        /** 
         * @brief Constructeur avec paramètre de couleur.
         * 
         * @param color Caractère de couleur à définir pour initialiser ce pion.
         **/
        Piece(char color);

        /** 
         * @brief Getter permettant de récupérer la couleur du pion correspondant.
         * 
         * @return caractère : la couleur du pion.
         **/
        char getColor();

        /** @brief Permet de changer rapidement la couleur d'un pion. **/
        void changeColor();
};