#include "Piece.hpp"

Piece::Piece(){
    color = '.';
};

Piece::Piece(char color) : color(color){};

char Piece::getColor(){
    return color;
};

void Piece::changeColor(){
    /** Simple opération de changement de couleur. */
    if(getColor() =='W'){
        color = 'B';
    }else{
        if(getColor() =='B'){
            color = 'W';
        }
    }
}