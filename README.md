# L3_INFO_Reversi

Projet de CPP | GL sur le jeu Riversi (ou Othello).


[Règles du jeu](http://www.ffothello.org/othello/regles-du-jeu/)

## Comment lancer le jeu

[Accessible ici](http://arthe1612.gitlab.io/l3_info_reversi/)

## Comment jouer au jeu

Le jeu se joue à deux joueurs.
Chaque joueur possède 62 pièces de couleur.

Le joueur 1 possède des pions noirs.
Le joueur 2 possède des pions blancs.


Lors du lancement de la partie :

Le premier joueur (l'utilisateur) place un pion en choisissant parmi les placements de pions possible (symbolisés par des petits pions bleus sur la grille).

Un placement de pion est valide uniquement si le joueur en question est sur le point de "capturer" un pion adverse.

Pour capturer un pion adverse, le joueur doit borner un (ou plusieurs) pion(s) de couleurs adverse avec ses propres pions de couleur.
A noter qu'il est possible de borner dans 8 directions différentes depuis le pion placé.


Une fois le pion placé, l'IA va, à son tour, choisir son placement de pion. Et ainsi de suite.
l'IA par défaut est une IA de Monte Carlo paramétrée sur 32 occurences de parties.
(Pour le moment, il n'est pas possible de modifier ce paramètre sans recompiler le projet)

Si l'un des joueurs n'est plus capable de placer un pion, son tour est passé automatiquement.

La partie se termine lorque les deux joueurs ne sont plus capables de placer un pion.
Elle se termine également si L'UN des joueurs ne possède plus de pions (cas très rare).

Le gagnant est le joueur possèdant le plus de pions sur la grille.

## Architecture projet

Le projet contient plusieurs dossiers :

- core :
  - cmake : contient un fichier cmake permettant d'importer plus facilement des dépendances.
  - fonts : contient la police d'écriture nécéssaire à la compilation
  - src : contient le code source du projet
  - CMakeLists.txt : configuration CMake projet
- doxygen :
  - Doxyfile : Fichier de configuration pour création de la doc
- emsdk :
  - Sous module Git permettant d'utiliser emscripten
- public : 
  - icon.png : Icone du site web
  - index.html : fichier index.html du site web
  - style.css : StyleSheet css
- run :
  - run.sh : Script IA vs IA
- uml :
  - classDiagram.png : Représentation - Diagramme de classe

## Compilation c++
Si vous souhaitez compiler et éxécuter les programmes c++ (cli/gui/ia).

### Dépendances requises :
cmake libsdl2-dev libsdl2-ttf-dev libsdl2-gfx-dev cpputest pkgconf gnuplot

### Etapes à suivre :

1. Télécharger le dépot git projet
2. Se rendre dans le répertoire ./core
3. Exécuter les lignes suivantes dans l'ordre :
   - mkdir build
   - cd build
   - mkdir assets
   - cp ../fonts/roboto.ttf assets/
   - cmake ..
   - make
4. Lancer l'executable de votre choix :
   - ./cli
   - ./gui
5. Pour lancer la comparaison d'IA depuis le répertoire core/build
   - cd ../../run
   - ./run.sh
6. Le graphique de comparaison IA vs IA est alors accessible par défaut ici :
   - ./run/

## Affichage de la comparaison d'IA par défaut :

![alt text](https://arthe1612.gitlab.io/l3_info_reversi/doc/run/out-mc2-mc.png)

## Diagramme de classe projet :

![alt text](https://arthe1612.gitlab.io/l3_info_reversi/doc/uml/classDiagram.png)


## Fonctionnalités disponibles :

Le projet propose trois exécutables et un jeu web.

Les executables sont situés dans le répertoire projet : /core/build/
- ./cli lance le reversi en ligne de commande
- ./gui lance le reversi en affichage graphique
- ./unitTest lance les tests unitaires du moteur de jeu.

Il est également possible de jouer au jeu graphique depuis ce lien web :
[Jeu](http://arthe1612.gitlab.io/l3_info_reversi/)

L'executable cli se base sur le moteur de jeu, relié à une IA.
L'executable gui se base sur un wrapper du moteur de jeu portant le nom de gui.

unitTest, lui, n'utilise que les classes du moteur de jeu basiques (board, piece et player).

Dans sa dernière version, le jeu intégre une IA de type Monte Carlo.
Ainsi, le jeu ne peut plus se jouer en multijoueur comme c'était le cas auparavant.

Pour fonctionner, le jeu se base sur une structure objet.
Nous retrouvons les trois classes de notre moteur de jeu : Piece, Player et Board.
Ces trois classes sont utilisés par la classe Game pour implémenter le jeu en affichage console. (Jalon 1)

Cette même classe Game fait appel à une classe auxiliaire : IA, dont le but est de fournir des méthodes propres à l'intelligence artificielle. (Jalon 3)

La classe GUI hérite directement de Game.
Ainsi, le jeu graphique est une sorte de "wrapper" de Game. (Jalon 2)

Vis à vis des fonctionnalités plus spécifiques en jeu,
On retrouve par exemple le passage de tour automatique, le relancement de jeu automatique en mode graphique.
L'aide au placement de pions.


## Fonctionnalités manquantes :

La fonctionnalité manquante la plus importante à mon sens est certainement le choix de jouer contre une IA ou non.

L'implémentation de l'IA dans le jeu n'a été effectué que très tard (avant-dernière séance).
Par conséquent, même son implémentation n'est pas parfaite.
Elle est suffisante, mais une meilleure optimisation du modèle objet aurait été appréciée.
D'autant plus que son ajout a quelques peu rendu le diagramme de classe plus compliqué.
Il n'a pas été possible d'améliorer cela par manque de temps.

Dans la même optique, une plus grande personnalisation de l'IA était prévue, permettant de proposer à l'utilisateur plusieurs niveaux de difficultés.

A part cela, un code plus optimisé sous certains aspect était prévu mais ne sera pas réalisable dans les temps.
Par exemple, les fuites de mémoires causées par SDL2.


## Bugs connus :

Aucun bug connu à ce jour.
Néanmoins, il est probable que jouer plusieurs parties d'affilée ne soit pas une bonne idée.
Il semble qu'SDL2 génère des pointeurs et pour le moment, je n'ai pas réussi à tous les supprimer dès qu'inutilisé.

Par conséquent, le jeu finira certainement, au bout d'un moment, de devenir de plus en plus long.
Et le navigateur risque même de fermer l'application si cette dernière nécéssite trop de mémoire.
A noter que ce dernier cas de figure ne s'est pas encore produit en test.

Il s'agit surtout d'un phénoméne anticipé, compte tenu de la mémoire perdue à chaque coups.


## Retour d'expérience :

Ce projet a été plutot enrichissant vis à vis du développement c++ objet.
Cela a été très intéressant également vis à vis du génie logiciel.

L'utilisation d'issues et de milestones m'a également bien aidé pour mieux m'organiser.

Utiliser emscripten pour rendre notre jeu accessible en web est également très intéréssant.
l'élaboration d'une IA simple est agréable également.


## Problèmes :

Bien entendu, il y a eu quelques difficultés.

Mais la plupart du temps, il ne s'agissait pas de problèmes de développement.
Mais surtout de déploiement.

On retrouve un nombre assez impréssionnant de commits dans le projet, et une grande partie d'entre eux était destinés à résoudre des problèmes de configuration de déploiement.
Parmi les problèmes de déploiement, nous retrouvons par exemple la génération js de emscripten et son installation.
Ou encore la création d'un cmake permettant de gérer à la fois une compilation c++ basique et une compilation em++.

Vis à vis du développement, la plus grande difficulté se résumera à adapter notre jeu graphique à une utilisation web.
Quelques aménagements ont du être effectués pour que notre jeu soit capable de se débrouiller convenablement dans les deux cas de figure : gui local et gui web.

A part cela, il y a eu quelques difficultés liées aux fuites de mémoires.
Le code a été vérifié ligne par ligne pour trouver d'éventuels problèmes.
Désormais, des précautions ont été prises pour limiter les pertes.

Par exemple, en s'assurant que tout objet de surface/text et texture soit détruit après rendu.


## Conclusion :

Le projet était très intéréssant, le jeu est plutôt simple à comprendre, et son développement est agréable.
La mise en place d'une structure objet avant même de développer était très utile.

Malheureusement, elle manque de flexibilité à mon gout (et cela se ressent lors de la mise en place de l'IA).