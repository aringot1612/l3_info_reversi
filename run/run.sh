#!/bin/sh

CMP_BIN="../core/build/ia"
NSIMSS="2 4 8 16 32 64"
NRUNS=100
CSVFILE="out-mc2-mc.csv"
PNGFILE="out-mc2-mc.png"

rm -f ${CSVFILE}
for nsims in ${NSIMSS} ; do
    echo ""
    echo "running nsims=${nsims}"
    ${CMP_BIN} ${NRUNS} "mc" ${nsims} "mc" 2 | tail -n 1 >> ${CSVFILE}
done

gnuplot -e "set out '${PNGFILE}' ; \
    set terminal png size 640,360 ; \
    set datafile separator ';' ; \
    set style data linespoints ; \
    set grid xtics ytics ; \
    set yrange [0:1] ; \
    set xlabel 'mc nsims' ; \
    set ylabel '% mc wins' ; \
    set key bottom right ; \
    plot '${CSVFILE}' using 3:(\$6/${NRUNS}) title 'mc vs mc2' lw 2 "